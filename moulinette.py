import argparse
import colors

parser = argparse.ArgumentParser(description='Parse through filename given as\
    input.')
parser.add_argument("-d", help="run script on the directory D and all\
    subdirectories")
parser.add_argument("-f", help="run script on single F file")
args = parser.parse_args()

directory = "./"
filename = args.f

def tabs_emptylines(fd, cname):
  curr_l = 1
  for line in fd.readlines():
    for i in range(len(line)):
      if line[i] == '\t':
        print cname + ": " + \
              colors.c.BRED + "tabulation at line %d:" % curr_l + colors.c.END + \
              "\n%s" % line
      curr_l += 1
  if line == '\n':
    print cname + ": empty line at end of file"


def main(): # for all the files in the directory
  try:
    f = open(filename, 'r')
    cname = colors.c.BGREEN + "in file " + \
            colors.c.BYELLOW + "\'%s\'" % filename + colors.c.END
    #print colors.c.BGREEN + cname
    tabs_emptylines(f, cname)

    f.close()
  except IOError:
    print colors.c.BRED + "Error: " + colors.c.WHITE + "invalid file name:"
    print colors.c.END + "\'%s\'" % filename

main()
