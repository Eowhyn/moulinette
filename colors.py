class c:
  RED = '\033[31m'
  YELLOW = '\033[33m'
  BLUE = '\033[34m'
  GREEN = '\033[32m'
  PURPLE = '\033[35m'
  WHITE = '\033[37m'
  CYAN = '\033[36m'
  BOLD = '\033[1m'
  UL = '\033[4m'
  END = '\033[0m'
  BRED = BOLD + RED
  BYELLOW = BOLD + YELLOW
  BBLUE = BOLD + BLUE
  BGREEN = BOLD + GREEN
  BPURPLE = BOLD + PURPLE
  BWHITE = BOLD + WHITE
  BCYAN = BOLD + CYAN
